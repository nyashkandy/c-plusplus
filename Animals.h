#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void  Voice()
    {
        cout << "voice";
    }
};

class Dog :public Animal
{
public:
    void  Voice() override
    {
        cout << "woof \n";
    }
};

class Cat :public Animal
{
public:
    void  Voice() override
    {
        cout << "Meow \n";
    }
};

class Human :public Animal
{
public:
    void  Voice() override
    {
        cout << "kek lol \n";
    }
};